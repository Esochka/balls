package sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;


import javax.imageio.ImageIO;

public class Controller implements Initializable {

    public Label labe;
    public RadioButton fillRB;
    private GraphicsContext gcB;
    double startX, startY, lastX,lastY,oldX,oldY;

    @FXML private ColorPicker colorPick;
    @FXML private Canvas TheCanvas;
    @FXML private Slider sizeSlider;



    @FXML
    private void onMousePressedListener(MouseEvent e){
        this.startX = e.getX();
        this.startY = e.getY();
        this.oldX = e.getX();
        this.oldY = e.getY();

    }

    @FXML
    private void onMouseDraggedListener(MouseEvent e){
        this.lastX = e.getX();
        this.lastY = e.getY();
        freeDrawing();
    }

    @FXML
    private void onMouseReleaseListener(MouseEvent e){
        freeDrawing();
    }

    private void freeDrawing()
    {
        gcB.setLineWidth(sizeSlider.getValue());
        if (fillRB.isSelected()){
            gcB.setStroke(Color.rgb(242, 242, 242));
        }
        else {
            gcB.setStroke(colorPick.getValue());
        }
        gcB.strokeLine(oldX, oldY, lastX, lastY);
        oldX = lastX;
        oldY = lastY;

    }




    @FXML
    private void clearCanvas(ActionEvent e)
    {
        gcB.clearRect(0, 0, TheCanvas.getWidth(), TheCanvas.getHeight());
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gcB = TheCanvas.getGraphicsContext2D();
        sizeSlider.setMin(1);
        sizeSlider.setMax(50);

        sizeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                labe.setText(String.format("%.0f", newValue));
                gcB.setLineWidth(sizeSlider.getValue());
            }
        });


    }


    @FXML

    private void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"),
                new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.gif"));

        fileChooser.setInitialFileName("untitled");
       // fileChooser.setInitialDirectory(new File("C:\\Users\\Vlad)))\\Desktop\\PaitDesct"));
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                WritableImage writableImage = TheCanvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            } catch (IOException e) {
                System.out.println("Failed to save image" + e);
            }
        }
    }


    @FXML
    private void open(){
        FileChooser openFile = new FileChooser();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog(null);
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                gcB.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
    }




    @FXML
    private void onExit() {
        Platform.exit();
    }





}



package com.example.myapplication;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class Ball {
    private float bollRadius;
    private float ballX = this.bollRadius+this.bollRadius/4;
    private float ballY = this.bollRadius+this.bollRadius/2;
    private float ballSpeedX;
    private float ballSpeedY;
    private RectF ballBounds;
    private Paint paint;

    public Ball(float ballX,float ballY, float radius, int ballSpeedX, int ballSpeedY, int alpha, int red, int green, int blue){
        this.ballX=ballX;
        this.ballY=ballY;
        this.bollRadius= radius;

        this.ballSpeedX=ballSpeedX;
        this.ballSpeedY=ballSpeedY;

        this.ballBounds= new RectF();
        this.paint= new Paint();

        this.setColor(alpha,red,green,blue);
    }

    public Ball(float ballX, float ballY, float radius, int alpha, int red,int green, int blue){
        this.ballX=ballX;
        this.ballY=ballY;
        this.bollRadius= radius;

        this.ballBounds= new RectF();
        this.paint= new Paint();

        this.setColor(alpha,red,green,blue);
    }

    public void setColor(int alpha, int red, int green, int blue){
        this.paint.setARGB(alpha, red, green, blue);
    }

    public void setBallSpeed(float speedX,float speedY){
        this.ballSpeedX=speedX;
        this.ballSpeedY=speedY;
    }

    public void update (int xMin, int xMax, int yMin, int yMax){
        this.ballX +=this.ballSpeedX;
        this.ballY +=this.ballSpeedY;
        if (this.ballX+this.bollRadius > xMax){
            this.ballSpeedX =-this.ballSpeedX;
            this.ballX =xMax - this.bollRadius;
        }
        else
        if (this.ballX-this.bollRadius < xMin){
            this.ballSpeedX =-this.ballSpeedX;
            this.ballX =xMax + this.bollRadius;
        }

        if (this.ballY+this.bollRadius > yMax){
            this.ballSpeedY =-this.ballSpeedX;
            this.ballY =yMax - this.bollRadius;
        }
        else
        if (this.ballY-this.bollRadius < yMin){
            this.ballSpeedY =-this.ballSpeedX;
            this.ballY =yMax + this.bollRadius;
        }
    }

    public void draw(Canvas canvas){
        this.ballBounds.set(this.ballX-this.bollRadius, this.ballY-this.bollRadius, this.ballX+this.bollRadius,this.ballY+this.bollRadius);
        canvas.drawOval(this.ballBounds,this.paint);
    }

}
